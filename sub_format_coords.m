function str = sub_format_coords(lon,lat)
%generates formatted DMS coordinates based on lon/lat in decimal degrees

if lon < 0
   hem1 = 'W';
else
   hem1 = 'E';
end
if lat < 0
   hem2 = 'S';
else
   hem2 = 'N';
end

try
   str = sprintf(['%02d %02d %0.2f ',hem2,', %03d %02d %0.2f ',hem1],ddeg2dms(abs(lat)),ddeg2dms(abs(lon)));
catch
   str = '';
end