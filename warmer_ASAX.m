function hot_x = warmer_ASAX(s1, s2, locs, alphabet)
% warmer_ASAX provides the days in which sax_max (s1) and sax_min (s2) are
% both top of the alphabet from which have been made

% I'm not 'happy' with alphabet=5 for the current example AR00...
% as the candidates to heatwaves are too many consecutive days... 
% We shall tuning the alphabet size as next task.

index_comp = cellfun(@strcmp, s1, s2); % comparing alphabet of the 2 SAX-Q
int1 = cell(1, length(s1));
int1(index_comp) = s1(index_comp); % when both SAX-Q are the same... Which value SAX does take?

% letter depending on alphabet number...
top = char(alphabet + 'a' -1);

positionX = find(strcmp(int1,top));
candidates = length(positionX); % number of candidates to heat wave

x_start = []; x_end = []; 
for i = 1 : candidates     
   x_start = [ x_start,  locs(positionX(i)) ];
   x_end   = [ x_end, locs(positionX(i)+1) ];
end;

% concatenate consecutive intervals
[~, ia, ib] = intersect(x_start, x_end);
x_start(ia) = [];
x_end(ib) = [];
        
hot_x = [x_start; x_end]';

% for i = 1 : size(hot_x,1)
%     if hot_x(i,2) - hot_x(i,1) > 12
%         series_zoom = dataYear(hot_x(i,1):hot_x(i,2) == ano, 8); % Just look to the maxima Tmax values to make it easier
%         [sax_max, locs] = adaptiveSAX1(series_zoom, alphabet, 3, 3); % keeping the same alphabet? minpeakdist = 3 minpeakh = 3?