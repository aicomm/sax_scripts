option = 1; %1-London datset and 2-Recife
% Starting to work for AIComm idea on multiresolution time series using SAX
% Let's see ...
if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;
if option == 1
   airt89 = dataYear(dataYear(:,1)==1989 & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
else
    airt89 = dataYear(dataYear(:,1)==1989 & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
    %airt89 = dataYear(dataYear(:,1)==1989 & (dataYear(:,2)<=3 | dataYear(:,2)>=10),9); % summer temperature 1989 in Recife
end
data = airt89;

% s = data;
d = size(data);

aux = [];
numCoeff = 61; % 1, 3, 
% for Recife summer: 2 3 7 13 78 546 (78)
% for London summer: 1 3 9 61 183 549 (61)
segLen = d(1)/numCoeff;  % assume it's integer
% sN = reshape(s, segLen, numCoeff);
sN = reshape(data, segLen, numCoeff);
avg = mean(sN);
dataPAA = repmat(avg, segLen, 1); % expand segments
dataPAA = dataPAA(:); % make column
aux = [aux dataPAA];

plot(data);
hold on;
plot(aux, 'LineWidth', 2 ); % in order to check if everything is going as expected

error1 = data - aux;
error2 = error1.^2;
rmsePAA = sqrt( sum(error2) / d(1) );