function features = features_ASAX1min(hot_x, ano, Tspeed, Tmonth, Tday, Thour)
% This function extracts features from the selected heat waves.

n_hw = size(hot_x,1);

start_hw = []; end_hw = []; y1 = []; y2 = []; 
maxi = []; mini = [];; maximin = []; avgi = []; avgimin = [];
duration = []; mini=[];anoi=[];minimin=[];

for i = 1:n_hw
    y1=[];
    x_hw = hot_x(i,1) : hot_x(i,2);
    start_hw = [start_hw; hot_x(i,1)]; 
    end_hw = [end_hw; hot_x(i,2)];
    y1 = [ y1; Tmax(x_hw) ];
    maxi = [maxi ; max(y1)];
    %avgi = [avgi; mean(y1)];
    mini = [mini ; min(y1)];
    avgi = [avgi; mean(y1)];
    duration = [duration; length(x_hw)];
    anoi = [anoi;ano];
    mesi = [mesi; Tmonth(hot_x(i,1))];
    diai = [diai; Tday(hot_x(i,1))];
    horai = [horai; Thour(hot_x(i,1))];
%     if duration < 3
%         p = [ p; 0 0 0];
%     else
%         p = [ p; polyfit(x_hw',Tmax(x_hw),2)];
%     end
end

% ano = repmat(anoi,n_hw,1);
% mes = repmat(mesi,n_hw,1);
% dia = repmat(diai,n_hw,1);
% hora = repmat(horai,n_hw,1);


features = [anoi, mesi, diai, horai, duration, start_hw, end_hw, maxi, mini, avgi];