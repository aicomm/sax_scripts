function [zoom_x, zoom_y, candidates] = WarmerSAXQ(x0, data_y, s1, s2, w_size)

index_comp = cellfun(@strcmp, s1, s2); % comparing alphabet of the 2 SAX-Q
int1 = cell(1, length(s1));
int1(index_comp) = s1(index_comp); % when both SAX-Q are the same... Which value SAX does take?

positionD = find(strcmp(int1, 'd')); % Let's guess alphabet_size = 4
candidates = length(positionD);

% take the selected segments where SAX_qu = SAX_ql = 'd'
x_start = []; x_end = []; 
for i = 1 : candidates     
   x_start = [ x_start, ((positionD(i)-1) * w_size + 1) ];
   x_end   = [ x_end, (x_start(i) + w_size - 1) ];
end;

zoom_x = []; zoom_y = [];
for i = 1 : length(x_start) 
   aux   = x_start(i) : x_end(i);
   zoom_x = [ zoom_x, x0 + aux' ];
   zoom_y = [ zoom_y , data_y(aux) ];
end;
