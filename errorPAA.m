option = 2; % 1-London dataset and 2-Recife

if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;

setdataL = 1961:2011;
indR = [1 6 8 11 23 29]; % to get rid of the years: 1961, 66, 68, 71, 83
setdataR = setdataL(setdiff(1:length(setdataL),indR));
setdataL = setdataL(setdiff(1:length(setdataL),40)); % something happens with 2001 ??

if option == 1
    nyears = length(setdataL);
else
    nyears = length(setdataR);
end;

rmseyears = []; m = []; sd = [];
for h = 1 : nyears
    if option == 1
        airt = dataYear(dataYear(:,1)== setdataL(h) & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for h_th year
    else 
        % airt = dataYear(dataYear(:,1)== setdataR(h) & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for h_th year
        airt = dataYear(dataYear(:,1)== setdataR(h) & (dataYear(:,2)<=3 | dataYear(:,2)>=10),9); % summer temperature h_th year in Recife
    end
    data = airt;
    year = h;
    m = [m mean(data)];
    sd = [sd std(data)];
    
    if option == 1
        nseg_propose = [ 1 3 9 61 183 ];
        % nseg1 = 9;  % value by default -> 61 segments
    else
        data = data(1:540,:); % unfortunately we cut off on Recife by the first 540 data per year
        nseg_propose = [ 1 3 9 60 180 ]; % sampling from 1,2,3,4,5,6,9,10,12,15,18,20,27,30,36,45,54,60,90,108,135,180,270,540,
        % nseg1 = 9; % value by default -> 60 segments
    end;
    
    d = size(data);
    rmsePAA = [];
    for i = 1 : length(nseg_propose)
        numCoeff = nseg_propose(i);
        segLen = d(1)/numCoeff;  % assume it's integer
        sN = reshape(data, segLen, numCoeff);
        avg = mean(sN);
        dataPAA = repmat(avg, segLen, 1); % expand segments
        dataPAA = dataPAA(:); % make column
        % aux = [aux dataPAA];
        error1 = data - dataPAA;
        error2 = error1.^2;
        rmsePAA = [ rmsePAA sqrt( sum(error2) / d(1) ) ];
    end;
    
    rmseyears = [ rmseyears; rmsePAA ];
end;
mean_temp = mean(m);
dev_temp = mean(sd);
mean_rmse = mean(rmseyears);