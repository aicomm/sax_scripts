function newTvalue = nan2mean(aStationTvalue)
%%
%% aStationTvalue
%% col 1 tdate, 
%% col 2 tyear, 
%% col 3 tmonth, 
%% col 4 tday, 
%% col 5 value (avg ou max or min)
%%
newTvalue                     = 0;
meanTvalue(1:12)              = zeros;
stdTvalue(1:12)               = zeros;
meanTvalue2(1:12)             = zeros;
stdTvalue2(1:12)              = zeros;
auxaStationTvalue             = aStationTvalue;
newaStationTvalue             = aStationTvalue;
%find the rows with nan in tavg and remove the rows
row_loc                       = any(isnan(auxaStationTvalue(:,5)),2);
auxaStationTvalue(row_loc,:)  = [];
data_outtavg                  = auxaStationTvalue;
%create mean values and std for each month
for nmonth = 1:12
    vmeanTmax(nmonth)         = mean(data_outtavg(data_outtavg(:,3) == nmonth,5));
    stdTvalue(nmonth)         = std(data_outtavg(data_outtavg(:,3) == nmonth,5));
    newaStationTvalue(newaStationTvalue(:,3) == nmonth & isnan(newaStationTvalue(:,5)),5) = vmeanTmax(nmonth);    
end;
newTvalue = newaStationTvalue(:,5);
% disp vmeanTmax
% disp stdTvalue
% 
% for nmonth = 1:12
%     vmeanTmax2(nmonth)         = mean(newaStationTvalue(newaStationTvalue(:,3) == nmonth,5));
%     stdTvalue2(nmonth)         = std(newaStationTvalue(newaStationTvalue(:,3) == nmonth,5));
% end;
% disp vmeanTmax2
% disp stdTvalue2


