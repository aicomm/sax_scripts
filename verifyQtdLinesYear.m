%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Verify the quantity in each year
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

option = 2; % 1-London dataset and 2-Recife

if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv4.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv4.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;
%Normal year 546. Bisexto 549
i = 1;
for h = 1961 : 2016
    dayyear = length(dataYear(dataYear(:,1)==h));
    daym1   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==1));
    daym2   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==2));
    daym3   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==3));
    daym4   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==4));
    daym5   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==5));
    daym6   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==6));
    daym7   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==7));
    daym8   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==8));
    daym9   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==9));
    daym10   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==10));
    daym11   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==11));
    daym12   = length(dataYear(dataYear(:,1)==h & dataYear(:,2)==12));
    airt = dataYear(dataYear(:,1)==h & (dataYear(:,2)<=3 | dataYear(:,2)>=10),9); % summer temperature h_th year in Recife
    lin = length(airt);
    %if lin ~= 546 & lin ~= 549
    if dayyear ~= 1095 & dayyear ~= 1098
       aux(i,1) = h;
       aux(i,2) = dayyear;
       aux(i,3) = 20;
       aux(i,4) = daym1;
       aux(i,5) = daym2;
       aux(i,6) = daym3;
       aux(i,7) = daym4;
       aux(i,8) = daym5;
       aux(i,9) = daym6;
       aux(i,10) = daym7;
       aux(i,11) = daym8;
       aux(i,12) = daym9;
       aux(i,13) = daym10;
       aux(i,14) = daym11;
       aux(i,15) = daym12;
       i = i+1;
    %elseif lin == 549 & mod(h,4) ~= 0
    elseif dayyear == 1098 & mod(h,4) ~= 0
            aux(i,1) = h;
            aux(i,2) = dayyear;
            aux(i,3) = 30;
            aux(i,4) = daym1;
            aux(i,5) = daym2;
            aux(i,6) = daym3;
            aux(i,7) = daym4;
            aux(i,8) = daym5;
            aux(i,9) = daym6;
            aux(i,10) = daym7;
            aux(i,11) = daym8;
            aux(i,12) = daym9;
            aux(i,13) = daym10;
            aux(i,14) = daym11;
            aux(i,15) = daym12;
            i = i+1;
%     elseif dayyear ~= 1095 & dayyear ~= 1098
%             aux(i,1) = h;
%             aux(i,2) = lin;
%             aux(i,3) = 40;
%             i = i+1;
%     elseif daym1 ~= 93 || daym3 ~= 93 || daym4 ~= 90 || daym5 ~= 93 || daym6 ~= 90 || daym7 ~= 93 ...
%             || daym8 ~= 93 || daym9 ~= 90 || daym10 ~= 93 || daym11 ~= 90 || daym12 ~= 93 || ...
%             (daym2 ~= 84 & daym2 ~= 87)
%             aux(i,1) = h;
%             aux(i,2) = lin;
%             aux(i,3) = 10;
%             i = i+1;
    end;   
    clear airt;
end;
airt;