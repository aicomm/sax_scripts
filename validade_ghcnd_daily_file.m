%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validade patterns in selected file stations
% 1- Each year must be completed (from 1/1/yyyy to 12/31/yyyy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;close all
fclose('all');
dirCGE          = 'D:\ghcnd_main_data'; %'C:\Users\AidaAraujo\Documents\gce_datatools_396b_public\gce_sample_data\ghcnd_all';
dirSAX          = 'C:\Users\AidaAraujo\Documents\sax_data';

rejectionsFile = fopen('rejections.txt','a');

dailyStationFile        = 'DailyStations.csv'; %'DailyStations.xlsx';
C                       = readtable('tbCountries.csv','Delimiter',',','ReadVariableNames',true);
countryId               = C.id;
countryName             = C.name;
TBStations              = readtable('tbStations.csv','Delimiter',',','ReadVariableNames',true);
parmlist                = table2struct(TBStations,'ToScalar',true);
stationidT              = parmlist.stationid;
latT                    = parmlist.lat;
longT                   = parmlist.long;

TBValidStations         = readtable('DailyStations_OLD.xlsx','ReadVariableNames',true);
ValidStations           = table2struct(TBValidStations,'ToScalar',true);
qtdValidStations        = size(TBValidStations,1);
template    = 'NCDC_GHCND';
datestart   = [];
dateend     = [];
silent      = 0; %suppress progress bar status updates if GUI mode (0 = no/default, 1 = yes)
deleteopt   = 1; %delete the temporary tabular file (0 = no, 1 = yes/default)

for ind = 1:qtdValidStations;
    nameValidFile = strcat(ValidStations.name{ind}, '.dly');    
    if exist(strcat(dirCGE, '\', nameValidFile), 'file') 
       [s,msg,s_raw] = imp_ncdc_ghcnd(nameValidFile,dirCGE,template,datestart,dateend,silent,deleteopt);
       %cdStation = unique(extract(s_raw,'Station'));
       cdStation     = unique(extract(s,'Station')); 
       idCountry     = cdStation{1}(1:2);
       linCountry    = strmatch(idCountry, countryId, 'exact'); 
       nmCountry     = countryName(linCountry);
       cdLin         = strmatch(cdStation, stationidT, 'exact'); 
       cdLat         = latT(cdLin,1); 
       cdLong        = longT(cdLin,1); 
              
       dt_str = extract(s_raw,'Date');
       %get numeric serial dates
       %dt_str = extract(s_raw,'Date');
       if mlversion >= 8
          %use format-specific datenum
          dt = datenum(dt_str,'mm/dd/yyyy');
       else
          %use generic datenum
          dt = datenum(dt_str);
       end
              
       %get date range
       dt_min   = min(no_nan(dt));
       iniYear  = str2num(datestr(dt_min,'yyyy')); 
       dt_max   = max(no_nan(dt));
       finalYear = str2num(datestr(dt_max,'yyyy')); 
       qtdYear  = finalYear - iniYear;
       
       %generate complete date range and formatted string version
       dt_full  = (dt_min:dt_max)';
       qtdDaysValid  = length(dt_full);
       qtdDays       = size(s.values{1});
       if ~(qtdDaysValid == qtdDays)
       end;
       cdate    = strmatch('Date', s.name, 'exact'); 
       cyear    = strmatch('Year', s.name, 'exact'); 
       cmonth   = strmatch('Month', s.name, 'exact'); 
       cday     = strmatch('Day', s.name, 'exact'); 
       ctavg    = strmatch('TAVG', s.name, 'exact'); 
       ctmax    = strmatch('TMAX', s.name, 'exact'); 
       ctmin    = strmatch('TMIN', s.name, 'exact');
       
       if length([cdate cyear cmonth cday ctavg ctmax ctmin])< 7
           fprintf(' ind:%i station: %s Country:%s ***Variables Missing!!!\n', ind, cdStation{1,1},  nmCountry{1});
           fprintf(rejectionsFile,' ind:%i station: %s Country:%s ***Variables Missing!!!\n', ind, cdStation{1,1},  nmCountry{1});
           continue;
       end
               
       tdate    = s.values{:,cdate}; 
       tyear    = s.values{:,cyear};
       tmonth   = s.values{:,cmonth};
       tday     = s.values{:,cday};
       %
       tavg     = s.values{:,ctavg};
       tmax     = s.values{:,ctmax};
       tmin     = s.values{:,ctmin};
       tlin     = length(tdate);
      
       %%%%%%%%
       % Transform values null (NAN) of TMAX, TMIN, TAVG in mean values
       %aStationTavg    = [table2array(tableStationTemperature(:,4:7)) tavg(:,1)];
       newTavg = nan2mean([tdate(:,1) tyear(:,1) tmonth(:,1) tday(:,1) tavg(:,1)]);
       
       %aStationTmax    = [table2array(tableStationTemperature(:,4:7)) tmax(:,1)];
       newTmax = nan2mean([tdate(:,1) tyear(:,1) tmonth(:,1) tday(:,1) tmax(:,1)]);
       
       %aStationTmin    = [table2array(tableStationTemperature(:,4:7)) tmin(:,1)];
       newTmin = nan2mean([tdate(:,1) tyear(:,1) tmonth(:,1) tday(:,1) tmin(:,1)]);
       %%%%%%%%%
       tstation (1:tlin, 1)          = cdStation; 
       tlatitude (1:tlin, 1)         = cdLat;
       tlongitude (1:tlin, 1)        = cdLong;
       tableStationTemperature       = table (tstation, tlatitude, tlongitude, tdate, tyear, tmonth, tday, newTavg,newTmax, newTmin); 
       dailyStationTemperatureFile   = strcat(dirCGE,'\novos\', cdStation{1,1}, '.csv');
       %writetable(tableStationTemperature,dailyStationTemperatureFile, 'FileType', 'spreadsheet');
       writetable(tableStationTemperature,dailyStationTemperatureFile, 'FileType','text','Delimiter', ',','QuoteStrings',true );
       clear tstation tlatitude  tlongitude  tdate  tyear  tmonth  tday  tavg tmax tmin; 
       str = sub_format_coords(cdLong,cdLat);
       fprintf(' ind:%i station: %s Country:%s coordinate:%s  ', ind, cdStation{1,1},  nmCountry{1}, str);
       fprintf(' lat:%f   long:%f  qtdYear:%i    \n',   cdLat, cdLong, qtdYear);
       %fprintf('station:%s %s \n', s.metadata{18,3}, s.metadata{19,3});
    end;
end;

