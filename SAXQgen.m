function [sax_qu, sax_ql, win_size] = SAXQgen(weather_data, nseg, alphabet_size) % nseg = 9 , alphabet_size = 4
% [a,b,wini] = SAXgen(array_data, 9, 4);
airty = weather_data;
data_len = length(airty);

if (mod(data_len, nseg))
    disp('nseg must be divisible by the data length. Aborting ');
    return;  
end;

win_size = floor(data_len/nseg);
data = (airty - mean(airty))/std(airty);

if data_len == nseg
    % PAA = data;
    PAAql =  data;
    PAAqu =  data;        
else
    % PAA = mean(reshape(data,win_size,nseg));
    PAAql = quantile(reshape(data,win_size,nseg),0.05);
    PAAqu = quantile(reshape(data,win_size,nseg),0.95);
end

%PAA_plot = repmat(PAA', 1, win_size);
%PAA_plot = reshape(PAA_plot', 1, data_len)';

PAAql_plot = repmat(PAAql', 1, win_size);
PAAql_plot = reshape(PAAql_plot', 1, data_len)';

PAAqu_plot = repmat(PAAqu', 1, win_size);
PAAqu_plot = reshape(PAAqu_plot', 1, data_len)';

dataql = (PAAql_plot - mean(PAAql_plot))/std(PAAql_plot);
dataqu = (PAAqu_plot - mean(PAAqu_plot))/std(PAAqu_plot);
strql = timeseries2symbol(dataql, data_len, nseg, alphabet_size);
strqu = timeseries2symbol(dataqu, data_len, nseg, alphabet_size);
% str = timeseries2symbol(data, data_len, nseg, alphabet_size);

switch alphabet_size
    case 2, cutlines  = [0];
    case 3, cutlines  = [-0.43 0.43];
    case 4, cutlines  = [-0.67 0 0.67];
    case 5, cutlines  = [-0.84 -0.25 0.25 0.84];
    case 6, cutlines  = [-0.97 -0.43 0 0.43 0.97];
    case 7, cutlines  = [-1.07 -0.57 -0.18 0.18 0.57 1.07];
    case 8, cutlines  = [-1.15 -0.67 -0.32 0 0.32 0.67 1.15];
    case 9, cutlines  = [-1.22 -0.76 -0.43 -0.14 0.14 0.43 0.76 1.22];
    case 10, cutlines = [-1.28 -0.84 -0.52 -0.25 0. 0.25 0.52 0.84 1.28];
    otherwise, disp('WARNING:: Alphabet size too big');
end;

symbols = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
    
sax_ql = symbols(strql);
sax_qu = symbols(strqu);

