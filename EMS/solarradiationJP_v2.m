%function It=solarradiation(Gsc,fi,Llog,roug)
%It=solarradiation(1367,39.15,121.7,0.2)

% Solarradiation.m is used to calculate the solar radiation for a digital elevation model integrated over one year. 
% Radiation is calculated depending on latitude, elevation, horizon shading, slope, aspect, 
% time of year and hour of the day (day length/season) and ground reflectance. 
% Temporal resolution of radiation calculation over sunshine hours can be set freely (1 hour default). 
% The script follows the approach by Kumar et al (1997). Simple unweighed 4 nearest neighbour gradient calculation is used and relief shading is not accounted for. 
% Reference: Kumar, L, Skidmore AK and Knowles E 1997: Modelling topographic variation in solar radiation in 
% a GIS environment. Int.J.Geogr.Info.Sys. 11(5), 475-497
dt_sol_1 = datetime(2017,6,21);
dt_sol_2 = datetime(2017,12,21);
indx_shift1 = (day(dt_sol_1,'dayofyear') * 24)+1;
indx_shift2 = (day(dt_sol_2,'dayofyear') * 24)+1;
opt_hemisphere = 2; %1 -> north or 2 south
Gsc=1367;% Gsc is the solar constant 1367W/m2
fi=07.100;% is the local geographical latitude JP φ %ignoring the sign because opt_hemisphere
Llog=034.867;%  is the local geographical longitude JP
roug=0.2;% is the ground albedo with 0.2 for ground and 0.7 for snow generallyJP = 0.2

for n=1:365
    Gon(1,n)=Gsc*(1+0.033*cosd(360*n/365));
    delta(1,n)=23.45*sind(360*(284+n)/365);
    omigas(1,n)=acosd(tand(delta(1,n))*(-tand(fi)));
    Ho(1,n)=24*3600*Gon(1,n)/pi*(cosd(fi)*cosd(delta(1,n))*sind(omigas(1,n))+2*pi*omigas(1,n)/360*sind(fi)*sind(delta(1,n)));
    N(1,n)=2*omigas(1,n)/15;
    %----------------------------------------%
    if n<=31 && n>=1
        n1=6.4;
    elseif n>=32 && n<=59
        n1=7.2;
    elseif n>=60 && n<=90
        n1=7.7;
    elseif n>=91 && n<=120
        n1=8.5;
    elseif n>=121 && n<=151
        n1=9.0;
    elseif n>=152 && n<=181
        n1=8.5;
    elseif n>=182 && n<=212
        n1=7.0;
    elseif n>=213 && n<=243
        n1=7.5;
    elseif n>=244 && n<=273
        n1=8.2;
    elseif n>=274 && n<=304
        n1=7.5;
    elseif n>=305 && n<=334
        n1=6.1;
    else
        n1=5.9;
    end
   
    
    if (n>=1 && n<=59) || (n>=305 && n<=365)
        Kt(1,n)=0.14+0.47*n1/N(n);
    elseif n>=152 && n<=243
        Kt(1,n)=0.24+0.4*n1/N(n);
    else
        Kt(1,n)=0.36+0.23*n1/N(n);
    end
   %-----------------------%
   
   H(1,n)=Ho(1,n)*Kt(1,n); %
   
   %------------%
   if Kt(1,n)<=0.17
       Hd(1,n)=0.99*H(1,n);
   elseif Kt(1,n)>0.17 && Kt(1,n)<0.75
        Hd(1,n)=(1.188-2.272*Kt(1,n)+9.473*(Kt(1,n))^2-21.865*(Kt(1,n))^3+14.648*(Kt(1,n))^4)*H(1,n);
   elseif Kt(1,n)>0.75 && Kt(1,n)<0.18
        Hd(1,n)=(0.632-0.54*Kt(1,n))*H(1,n);
   else
       Hd(1,n)=0.2*H(1,n);
   end
   %---------------%
   
   %-------------------------------------%
   for h=1:24
       m=24*(n-1)+h;
       tttt(1,m)=m;
       omiga(1,m)=15*((h*60+9.87*sind(2*360*(n-81)/364)-7.53*cosd(360*(n-81)/364)-1.5*sind(360*(n-81)/364)-4*(120-Llog))/60-12);%太阳时角的处理要注意，之前参考杨卫波硕士论文出现了
      
       if abs(omiga(1,m))<omigas(1,n)
          m1=m;
          m2=m+1;
          omiga(1,m1)=omiga(1,m);
          omiga(1,m2)=15*(((h+1)*60+9.87*sind(2*360*(n-81)/364)-7.53*cosd(360*(n-81)/364)-1.5*sind(360*(n-81)/364)-4*(120-Llog))/60-12);
          % I is Hourly global solar radiation on the horizontal surface
          I(1,m)=H(1,n)*pi/24*((0.409+0.5016*sind(omigas(1,n)-60))+(0.6609-0.4767*sind(omigas(1,n)-60))*cosd((omiga(1,m1)+omiga(1,m2))/2))*(cosd((omiga(1,m1)+omiga(1,m2))/2)-cosd(omigas(1,n)))/(sind(omigas(1,n))-2*pi*omigas(1,n)*cosd(omigas(1,n))/360);
          % Id is Hourly diffuse solar radiation on the horizontal surface
          Id(1,m)=Hd(1,n)*pi/24*(cosd((omiga(1,m1)+omiga(1,m2))/2)-cosd(omigas(1,n)))/(sind(omigas(1,n))-2*pi*omigas(1,n)*cosd(omigas(1,n))/360);
          % Rbave is Ratio of beam radiation on tilted surface to that on horizontal surface
          Rbave(1,m)=cosd(delta(1,n))*(sind(omiga(1,m2))-sind(omiga(1,m1)))/(cosd(fi)*cosd(delta(1,n))*(sind(omiga(1,m2))-sind(omiga(1,m1)))+sind(fi)*sind(delta(1,n))*pi/180*(omiga(1,m2)-omiga(1,m1)));
       else
          I(1,m)=0;
          Id(1,m)=0;
          Rbave(1,m)=0;
       end
       It(1,m)=I(1,m)*(roug*(1-cosd(fi))/2+Rbave(1,m))+Id(1,m)*((1+cosd(fi))/2-Rbave(1,m));% Hourly global solar radiation on the tilted surface
       if It(1,m)<0
           It(1,m)=0;
       end
       if I(1,m)<0
           I(1,m)=0;
       end
       if Id(1,m)<0
           Id(1,m)=0;
       end
   end
end   
%% If south hemisphere -> shift
if opt_hemisphere == 2
    for ind = 1:365
%         ind
%         indx_shift1
%         indx_shift2
        It_n(1,indx_shift2:indx_shift2+23)    = It (1,indx_shift1:indx_shift1+23); 
        I_n(1,indx_shift2:indx_shift2+23)     = I (1,indx_shift1:indx_shift1+23); 
        Id_n(1,indx_shift2:indx_shift2+23)    = Id (1,indx_shift1:indx_shift1+23); 
        Rbave_n(1,indx_shift2:indx_shift2+23) = Rbave(1,indx_shift1:indx_shift1+23);
        %tttt_n(1,indx_shift2)  = tttt(1,indx_shift2);
        indx_shift2 = indx_shift2 + 24; 
        indx_shift1 = indx_shift1 + 24; 
        if indx_shift1 > 8760
           indx_shift1 = 1;
        end;
        if indx_shift2 > 8760
           indx_shift2 = 1;
        end;
    end
    It    = It_n;
    I     = I_n;
    Id    = Id_n;
    Rbave = Rbave_n;
    %tttt  = tttt_n;
end;

subplot(2,2,1)
plot(tttt,It)
title('Hourly global solar radiation on the tilted surface')
xlabel('time(h)')
ylabel('It(J/m2)')
grid on  
hold on
subplot(2,2,2)
plot(tttt,I)
title('Hourly global solar radiation on the horizontal surface')
xlabel('time(h)')
ylabel('I(J/m2)')
grid on
hold on
subplot(2,2,3)
plot(tttt,Id)
title('Hourly diffuse solar radiation on the horizontal surface')
xlabel('time(h)')
ylabel('Id(J/m2)')
grid on
subplot(2,2,4)
plot(tttt,Rbave)
title('Ratio of beam radiation on tilted surface to that on horizontal surface')
xlabel('time(h)')
ylabel('Rbave')
grid on
dirData = 'C:\Users\AidaAraujo\Documents\sax_scripts\EMS\Data\JPRadiacao2007-2016';
nameFileJPSolarRad = [dirData '\' 'jpsolarrad.mat'];
save(nameFileJPSolarRad);
