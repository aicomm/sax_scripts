%INMET - Codes from http://www.inmet.gov.br/projetos/rede/pesquisa/form_mapas_c_horario.php
%Wind directions in degree - http://climate.umn.edu/snow_fence/components/winddirectionanddegreeswithouttable3.htm
%INMET                              Cardinal direction    Degree direction
%0	Calma	C                       N                     348.75 - 11.25
%1	Norte - Nordeste	NNE         NNE                   11.25 - 33.75
%2	Norte - Nordeste	NNE         NNE                   11.25 - 33.75
%3	Norte - Nordeste	NNE         NNE                   11.25 - 33.75
%4	Norte - Nordeste	NNE         NNE                   11.25 - 33.75
%5	Nordeste	NE                  NE                    33.75 - 56.25
%6	Nordeste	NE                  NE                    33.75 - 56.25
%7	Este - Nordeste	ENE             ENE                   56.25 - 78.75
%8	Este - Nordeste	ENE             ENE                   56.25 - 78.75
%9	Leste	E                       E                     78.75 - 101.25
%10	Leste	E                       E                     78.75 - 101.25
%11	Este - Sudeste	ESE             ESE                   101.25 - 123.75
%12	Este - Sudeste	ESE             ESE                   101.25 - 123.75
%13	Este - Sudeste	ESE             ESE                   101.25 - 123.75
%14	Sudeste	SE                      SE                    123.75 - 146.25
%15	Sudeste	SE                      SE                    123.75 - 146.25
%16	Sul - Sudeste	SSE             SSE                   146.25 - 168.75
%17	Sul - Sudeste	SSE             SSE                   146.25 - 168.75
%18	Sul	S                           S                     168.75 - 191.25
%19	Sul	S                           S                     168.75 - 191.25
%20	Sul - Sudoeste	SSW             SSW                   191.25 - 213.75
%21	Sul - Sudoeste	SSW             SSW                   191.25 - 213.75
%22	Sul - Sudoeste	SSW             SSW                   191.25 - 213.75
%23	Sudoeste	SW                  SW                    213.75 - 236.25
%24	Sudoeste	SW                  SW                    213.75 - 236.25
%25	Oeste - Sudoeste	WSW         WSW                   236.25 - 258.75
%26	Oeste - Sudoeste	WSW         WSW                   236.25 - 258.75
%27	Oeste	W                       W                     258.75 - 281.25
%28	Oeste	W                       W                     258.75 - 281.25
%29	Oeste - Noroeste	WNW         WNW                   281.25 - 303.75
%30	Oeste - Noroeste	WNW         WNW                   281.25 - 303.75
%31	Oeste - Noroeste	WNW         WNW                   281.25 - 303.75
%32	Noroeste	NW                  NW                    303.75 - 326.25
%33	Noroeste	NW                  NW                    303.75 - 326.25
%34	Norte - Noroeste	NNW         NNW                   326.25 - 348.75
%35	Norte - Noroeste	NNW         NNW                   326.25 - 348.75
%36	Norte	N                       N                     348.75 - 11.25
%99	Vari�vel	Vari�ve


%Table to convert wind direction in degree to code(JoaPessoa = London) or INMETCode(Recife)
% Code  Direction   Degreen                      INMET Code
%    1     N           348.75 - 11.25                0, 36
%    2     NNE         11.25 - 33.75                 1, 2, 3, 4
%    3     NE          33.75 - 56.25                 5, 6
%    4     ENE         56.25 - 78.75                 7, 8
%    5     E           78.75 - 101.25                9, 10
%    6     ESE         101.25 - 123.75               11, 12, 13
%    7     SE          123.75 - 146.25               14, 15
%    8     SSE         146.25 - 168.75               16, 17
%    9     S           168.75 - 191.25               18, 19
%    10    SSW         191.25 - 213.75               20, 21, 22
%    11    SW          213.75 - 236.25               23, 24
%    12    WSW         236.25 - 258.75               25, 26
%    13    W           258.75 - 281.25               27, 28
%    14    WNW         281.25 - 303.75               29, 30, 31
%    15    NW          303.75 - 326.25               32, 33
%    16    NNW         326.25 - 348.75               34, 35
clear all;close all; clc
dirData = 'C:\Users\AidaAraujo\Documents\sax_scripts\EMS\Data';
nameFileV2 = [dirData '\' 'JoaoPessoa_v2.csv'];
nameFileV3 = [dirData '\' 'JoaoPessoa_v3.csv']
%JoaoPessoa v2 -> criar coluna 11 codwdir
   Orig   = csvread(nameFileV2,1,0); 
   
   %Read header
   fid     = fopen(nameFileV2);
   a       = textscan(fid,'%s','Delimiter','\n');
   header  = a{1}(1);
   header  = strcat(header, ',codwdir');
   fclose(fid);
   
   joaopessoaV2     = csvread(nameFileV2,1,0);  
   dataLenOrig  = length(Orig);
   dataLenV2    = length(joaopessoaV2);
   if dataLenOrig == dataLenV2
      cont      = 0;
      for ind = 1:dataLenOrig
          %Create a new column (11) in  joaopessoaV2 with code wind direction
          %beteween [0,16] based on column(5-wdir) of joaopessoaV2
         if joaopessoaV2(ind,5) >= 348.75 ||  joaopessoaV2(ind,5) < 11.25
                 joaopessoaV2(ind, 11) = 1;
         elseif joaopessoaV2(ind,5) < 33.75
             joaopessoaV2(ind, 11) = 2;
         elseif joaopessoaV2(ind,5) < 56.25
             joaopessoaV2(ind, 11) = 3;
         elseif joaopessoaV2(ind,5) < 78.75
             joaopessoaV2(ind, 11) = 4;
         elseif joaopessoaV2(ind,5) < 101.25
             joaopessoaV2(ind, 11) = 5;
         elseif joaopessoaV2(ind,5) < 123.75
             joaopessoaV2(ind, 11) = 6;
         elseif joaopessoaV2(ind,5) < 146.25
             joaopessoaV2(ind, 11) = 7;
         elseif joaopessoaV2(ind,5) < 168.75
             joaopessoaV2(ind, 11) = 8;
         elseif joaopessoaV2(ind,5) < 191.25
             joaopessoaV2(ind, 11) = 9;
         elseif joaopessoaV2(ind,5) < 213.75
             joaopessoaV2(ind, 11) = 10;
         elseif joaopessoaV2(ind,5) < 236.25
             joaopessoaV2(ind, 11) = 11;
         elseif joaopessoaV2(ind,5) < 258.75
             joaopessoaV2(ind, 11) = 12;
         elseif joaopessoaV2(ind,5) < 281.25 
             joaopessoaV2(ind, 11) = 13;
         elseif joaopessoaV2(ind,5) < 303.75
             joaopessoaV2(ind, 11) = 14;
         elseif joaopessoaV2(ind,5) < 326.25
             joaopessoaV2(ind, 11) = 15;
         elseif joaopessoaV2(ind,5) < 348.75
             joaopessoaV2(ind, 11) = 16;
         end;
         cont = cont + 1;
         if cont > 1000
             disp (cont)
             cont = 0;
         end;
      end;
      disp(cont)
      csvwrite_with_headers(nameFileV3,joaopessoaV2,header) 
   else
       disp ('Invalid files!')
   end;


