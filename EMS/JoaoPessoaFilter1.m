%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Read header
% JoaoPessoa_v1 columns
%    1 Year ->  [1973, ]
%    2 Month -> [ 1, 12]
%    3 Day   -> [1, 31]
%    4 Hour  -> [0, 23]
%    5 wdir  -> [0,360] DIR = WIND DIRECTION IN COMPASS DEGREES
%    6 wspeed -> knots (SPD = WIND SPEED IN MILES PER HOUR)
%    7 cloud  -> Criar cloud cover maximo
%			  =M�XIMO(I3:K3) Valor maximo entre L, M e H, 0 -> 1, 9 -> 8
%    8 airp -> from SLP = SEA LEVEL PRESSURE IN MILLIBARS TO NEAREST TENTH 
%    9 airt -> Celsius TEMP = TEMPERATURE IN FAHRENHEIT 
%    10 dpt -> Celsius DEWP = DEW POINT IN FAHRENHEIT 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;close all;clc
setdbprefs('NullNumberRead');
fprintf ('\n Lendo arquivo');
dirData = 'C:\Users\AidaAraujo\Documents\sax_scripts\EMS\Data';
nameFileV1 = [dirData '\' 'JoaoPessoa_v1.csv'];
nameFileV2 = [dirData '\' 'JoaoPessoa_v2.csv']
fid = fopen(nameFileV1);
a = textscan(fid,'%s','Delimiter','\n');
header = a{1}(1);
fclose(fid);

%Filer file
max_temp  = 45;
min_temp  = 12;
mean_temp = [27.1  27.2  27  26.7  26  25.2  24.2  24.3  25.1  26.3  26.7  26.9];
dataYear  = csvread(nameFileV1,1,0);  
data_len  = length(dataYear);
contnanairt = 0;
continvairt = 0;
contnandir  = 0;
contnanspeed = 0;
contnancloud = 0;
contnanairp  = 0;
contnandpt   = 0;
cont = 0;
fprintf ('\n Corrigindo o arquivo. Total de registros: %i', data_len);
for ind = 1:data_len
    cont = cont + 1;
    if cont > 1000
        fprintf ('\n Total de registros lidos: %i', ind);
        cont = 0;
    end
    nyear  = dataYear(ind,1);
    nmonth = dataYear(ind,2);
    nhour  = dataYear(ind,4);
    
    %1 - Changing NaN temp for mean temp in year/month/hour or wiki
    %if strcmp(dataYear(ind,9),'NaN')
    if isnan(dataYear(ind,9))
        aux_temp     = dataYear(dataYear(:,1)== nyear & dataYear(:,2) == nmonth & dataYear(:,4) == nhour & dataYear(:,9) >  min_temp & dataYear(:,9) <  max_temp , 9);
        aux_tempmean = mean(aux_temp, 'omitnan');
        if aux_tempmean >  min_temp && aux_tempmean < max_temp
           dataYear(ind, 9) = aux_tempmean;
        else
           dataYear(ind, 9) = mean_temp(1,nmonth);
        end;
        contnanairt = contnanairt +1;
    end;
    %2 - Changing impossible temp for mean temp in year/month/hour or wiki
    if dataYear(ind,9) < min_temp || dataYear(ind,9) > max_temp 
        aux_temp     = dataYear(dataYear(:,1)== nyear & dataYear(:,2) == nmonth & dataYear(:,4) == nhour & dataYear(:,9) >  min_temp & dataYear(:,9) <  max_temp , 9);
        aux_tempmean = mean(aux_temp, 'omitnan');
        if aux_tempmean >  min_temp && aux_tempmean < max_temp
           dataYear(ind, 9) = aux_tempmean;
        else
           dataYear(ind, 9) = mean_temp(1,nmonth);
        end;
        continvairt = continvairt +1;
    end;
    %3- Changing NaN wdir for before wdir
    %if strcmp(dataYear(ind,5),'NaN') && ind > 1
    if isnan(dataYear(ind,5)) && ind > 1
       dataYear(ind,5) = dataYear(ind-1,5);
       contnandir = contnandir +1;
    end;
    %4- Changing NaN wspeed for before wspeed
    %if strcmp(dataYear(ind,6),'NaN') && ind > 1
    if isnan(dataYear(ind,6)) && ind > 1
       dataYear(ind,6) = dataYear(ind-1,6);
       contnanspeed = contnanspeed +1;
    end;
    %5- Changing NaN cloud for before cloud
    %if strcmp(dataYear(ind,7),'NaN') && ind > 1
    if isnan(dataYear(ind,7)) && ind > 1
       dataYear(ind,7) = dataYear(ind-1,7);
       contnancloud = contnancloud +1;
    end;
    %6- Changing NaN airp for before airp
    %if strcmp(dataYear(ind,8),'NaN') && ind > 1
    if isnan(dataYear(ind,8)) && ind > 1
       dataYear(ind,8) = dataYear(ind-1,8);
       contnanairp = contnanairp +1;
    end;
    %7- Changing NaN dpt for before dpt
    %if strcmp(dataYear(ind,10),'NaN') && ind > 1
    if isnan(dataYear(ind,10)) && ind > 1
       dataYear(ind,10) = dataYear(ind-1,10);
       contnandpt = contnandpt +1;
    end;
end
fprintf ('\n Corre��o finalizada');
fprintf ('\n airt nan: %i, airt wrong: %i', contnanairt, continvairt);
fprintf ('\n dir nan: %i, speed nan: %i, cloud nan: %i', contnandir, contnanspeed, contnancloud);
fprintf ('\n airp nan: %i, dpt nan: %i', contnanairp, contnandpt);
%csvwrite('recife3linesv2.csv',dataYear);  
%Write with header
csvwrite_with_headers(nameFileV2,dataYear,header)

