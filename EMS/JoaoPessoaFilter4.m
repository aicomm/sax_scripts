%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Read header
% JoaoPessoa_v1 columns
%    1 Year ->  [1973, ]
%    2 Month -> [ 1, 12]
%    3 Day   -> [1, 31]
%    4 Hour  -> [0, 23]
%    5 wdir  -> [0,360] DIR = WIND DIRECTION IN COMPASS DEGREES
%    6 wspeed -> knots (SPD = WIND SPEED IN MILES PER HOUR)
%    7 cloud  -> Criar cloud cover maximo
%			  =M�XIMO(I3:K3) Valor maximo entre L, M e H, 0 -> 1, 9 -> 8
%    8 airp -> from SLP = SEA LEVEL PRESSURE IN MILLIBARS TO NEAREST TENTH 
%    9 airt -> Celsius TEMP = TEMPERATURE IN FAHRENHEIT 
%    10 wbtemp - NaN
%    11 dpt -> Celsius DEWP = DEW POINT IN FAHRENHEIT
%    12 codwdir - > [1,16] descrito no script JoaPessoaWindDirFilter2
%    13 - It - Hourly global solar radiation on the tilted surface
%    14 - I - Hourly global solar radiation on the horizontal surface
%    15 - Id - Hourly diffuse solar radiation on the horizontal surface
%    16 - Rbave - Ratio of beam radiation on tilted surface to that on horizontal surface
% Include columns about solar radiation (It, I, Id, Rbave)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 clear all;close all;clc
%setdbprefs('NullNumberRead');
opt_hemisphere = 2;
fi=07.100;% is the local geographical latitude JP �%ignoring the sign because opt_hemisphere
Llog=034.867;%  is the local geographical longitude JP| %ignoring the sign because opt_hemisphere
[It, I, Id, Rbave] = solarradiationJP_v3(fi,Llog, opt_hemisphere);
fprintf ('\n Lendo arquivo');
dirData = 'C:\Users\AidaAraujo\Documents\sax_scripts\EMS\Data';
nameFileV1 = [dirData '\' 'JoaoPessoa_v5.csv'];
nameFileV2 = [dirData '\' 'JoaoPessoa_v6.csv'];
fid = fopen(nameFileV1);
a = textscan(fid,'%s','Delimiter','\n');
header = a{1}(1);
fclose(fid);
header = strcat(header, ',It,I,Id,Rbave');


dataYear  = csvread(nameFileV1,1,0);  
data_len  = length(dataYear);
a_ini   = 1973;
t_ini   = datetime(a_ini,01,1,1,0,0);
a_final = 2016;
t_final = datetime(a_final,12,31,23,0,0);
t = t_ini:t_final;
[Y,M,D,H,mm, ss] = datevec(t);
dateComplete(:,1) = Y';
dateComplete(:,2) = M';
dateComplete(:,3) = D';
[lin, col] = size (dateComplete);
ind_ini = 1;
ind_final = 23;
indseq = 0;
ini = 1;
for ind_a= a_ini:a_final
    dt_end_year = datetime(ind_a,12,31);
    qtd_days = day(dt_end_year,'dayofyear');
    fin = ini -1 + (24*365); 
    dataYear(ini:fin, 13) = It(:);
    dataYear(ini:fin, 14) = I(:);
    dataYear(ini:fin, 15) = Id(:);
    dataYear(ini:fin, 16) = Rbave(:);
    if qtd_days > 365 % Leap year
       % ind_a
       fin = fin+1;
       dataYear(fin:fin+23, 13) = It(end-23:end);
       dataYear(fin:fin+23, 14) = I(end-23:end);
       dataYear(fin:fin+23, 15) = Id(end-23:end);
       dataYear(fin:fin+23, 16) = Rbave(end-23:end);
       fin = fin + 23;
    end;
    ini = fin+1; 
end;
%%% For 2017, 01, 01 23 hours
dataYear(fin+1:fin+24, 13) = It(end-23:end);
dataYear(fin+1:fin+24, 14) = I(end-23:end);
dataYear(fin+1:fin+24, 15) = Id(end-23:end);
dataYear(fin+1:fin+24, 16) = Rbave(end-23:end);

        
        

fprintf ('\n Corre��o finalizada');

%Write with header
csvwrite_with_headers(nameFileV2,dataYear,header);

