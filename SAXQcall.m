%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Importing data from weather stations. 1-London dataset and 2-Recife
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear all; clc
option = 1; % 1-London dataset and 2-Recife
nyears2 = 0;
cont = 0;
if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv2.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;

nyears = 50;
setdataL = 1961:2011;
indR = [1 6 8 11 23 29]; % to get rid of the years: 1961, 66, 68, 71, 83
setdataR = setdataL(setdiff(1:length(setdataL),indR));
if option == 2
    nyears = length(setdataR)-1;
end;
%setdataL = setdataL(setdiff(1:length(setdataL),40)); % something happens with 2001 ??
for h = 1 :nyears
    if option == 1
        airt = dataYear(dataYear(:,1)== setdataL(h) & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for h_th year
    else 
        % airt = dataYear(dataYear(:,1)== setdataR(h) & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for h_th year
        % airt = dataYear(dataYear(:,1)== setdataR(h) & (dataYear(:,2)<=3 | dataYear(:,2)>=10),9); % summer temperature h_th year in Recife
        %disp ([num2str((setdataR(h)+1)),',' , num2str(setdataR(h+1))])
          if (setdataR(h)+1) == setdataR(h+1)
             airt = dataYear((dataYear(:,1)== setdataR(h) & dataYear(:,2)>=10) | (dataYear(:,1)== setdataR(h+1) & dataYear(:,2)<=3),9); % summer temperature h_th year in Recife
          end
    end
    data = airt;
    year = h;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if option == 1
        nseg1 = 9;  % number of segments for PAA approach: 1 3 9 61 183 549  (61)
    else
        data = data(1:540,:); % unfortunately we cut off on Recife by the first 540 data per year
        nseg1 = 18;  % number of segments for PAA approach in the case of Recife summer: 2 3 7 13 78 546 (78)
    end;
    alphabet_size  = 4;  % alphabet size for SAX
    
    [SAXQup, SAXQlo, win_size1] = SAXQgen(data, nseg1, alphabet_size);
    
    [zoom_x1, zoom_y1, candidates1] = WarmerSAXQ(0, data, SAXQup, SAXQlo, win_size1);
    
    if option == 1
        zoom_x1 = zoom_x1(1:size(zoom_x1,1)-1,:); zoom_y1 = zoom_y1(1:size(zoom_y1,1)-1,:); % get rid of the 61th element to better work with 60
    else
        zoom_x1 = zoom_x1(1:size(zoom_x1,1),:); zoom_y1 = zoom_y1(1:size(zoom_y1,1),:);
    end;
    nseg2 = 6;  % number of segments for PAA approach: 1 2 3 4 5 6 10 12 15 20 30 60
    if ~isempty(zoom_x1) %when there is at least on heat wave candidate
        for i = 1 : candidates1 % size(zoom_x1,2)
            aux_y = zoom_y1(:,i); % aux_y = zoom_y1(:,1);
            aux_x = zoom_x1(:,i);
            [SAXQupr, SAXQlor, win_size2] = SAXQgen(aux_y, nseg2, alphabet_size);
            [zoom2_x1, zoom2_y1, candidates2] = WarmerSAXQ(aux_x(1)-1, aux_y, SAXQupr, SAXQlor, win_size2);
            HW.(['year' num2str(h)]) = cell(1,candidates2);
            for j = 1: candidates2
                HW.(['year' num2str(h)]){j} = [zoom2_x1(:,j), zoom2_y1(:,j)];
            end;
        end;
    else
        HW.(['year' num2str(h)]) = cell(1,candidates1);
    end;
    if option == 1
        s1 = 'LondonHW';
        s2 = int2str(year);
        s3 = '.mat';
        s  = strcat(s1,s2,s3);
        save(s, 'HW');  % save s HW
    else
        s1 = 'RecifeHW';
        s2 = int2str(year);
        s3 = '.mat';
        s  = strcat(s1,s2,s3);
        save(s, 'HW');
        nyears2 = nyears2 +1;
    end
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% For further analysis load(s) to have access to the saved environment
%% through, for instance, HW.year1989{1}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if option == 1
    nyears2 = length(fieldnames(HW));
else
    %nyears2 = 45;
    disp (nyears2)
end;    
    
x = []; y = []; noc = []; yearcount = []; ym = [];
for i = 1 : nyears2
    aux_year = 1960 + i;
    if option == 2 && (i == 8 || i == 17)
        %% nada y1 = [y1 0]
    else
        saux = strcat('year',int2str(i));
        value = getfield(HW, saux);
        if  isempty(value)
              disp(['Year ', saux, 'is empty of candidates.']); 
              noc = [noc 0];
        else
            for h = 1 : size(value,2)
                  indyear1 = value{1}(:,1);
                  indyear2 = value{1}(:,2);
                  x = [ x; indyear1 ];
                  y =[ y; indyear2 ]; 
                  yearcount = [ yearcount aux_year];
                  ym = [ ym; mean(indyear2) ];
            end;
            noc = [ noc size(value,2) ];
        end;
    end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% histograms of when
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(1)
% if option == 2
%      x(x>360) = x(x>360) - 540; % x is coming now from the initial point to 230
% end;

nbins = 12;
h = histogram(x,nbins,'Normalization','probability');
xlabel('Date'); %: from early June to early September');
ylabel('Probability of warmest temperatures');
if option == 1
    set(gca,'XTickLabel',{'1 June' '15 June' '1 July' '15 July' '1 August' '15 August' '1 Sept.'} );
else
    set(gca,'XTickLabel',{'1 Nov.' '15 Nov.' '1 Dec.' '15 Dec.' '1 Jan.' '15 Jan.' '1 Feb.'} );
end;
figure(2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% more stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if option == 1
    auxmed = 1:10:length(y);
else
    auxmed = 1:5:length(y);
end;

media = [];
for i = 1 : (length(auxmed) - 1)
    aux = y(auxmed(i):(auxmed(i+1) -1));
    media = [media mean(aux)];
end;
plot(media)
xlabel('Candidates to heatwaves sorted in time');
ylabel('Average temperature');

% noc2 = cumsum(noc);
% plot(noc2)
% hold
% plot(1:40, 1:40)
% legend('observed heatwaves','1 heatwave per year')
figure(3)
mymap = [1 0.5 0.5
    1 0.1 0
    0.5 0 0];
colormap(mymap) %colormap(jet(2))
noc1 = noc + 1;
% if option == 1
%     % nada noc1 = noc + 1;
% else
%     noc1 = [nan noc1(1:4) nan noc1(5:6) nan noc1(7:9) nan noc1(10:21) nan noc1(22:length(noc1))]; % considering missing years
% end;
image(noc1)
c = colorbar;
% ylabel(c,'Scale')
set(c,'YTickLabel',{'', '0' ,'' , '1', '' ,'>=2' ,''})
if option == 1
    set(gca,'XTickLabel',{'1970', '1980' ,'1990' , '2000', '2010'})
else
    % set(gca,'XTickLabel',{'1970' , '', '1980', '' , '1990', '', '2000', '', '2005'})
    set(gca,'XTickLabel',{'' , '1970', '', '1980' , '', '1990', '', '2000', '2005'})
end;
xlabel('Year');
ylabel('Candidates to heatwaves - Frequency');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% trend of heatwaves intensity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(4)
scatter(yearcount,ym,'filled')
xlabel('Year'); %: from early June to early September');
ylabel('Avg. temperature of heatwaves - Celsius');
aa = lsline;
set(aa(1),'color','r')
mdl = fitlm(yearcount,ym,'linear','RobustOpts','on');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% saving SAX-Qu and SAX-Ql alphabets for years with heatwave
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if option == 1
%     hotyears = unique(yearcount);
% else
%     hotyears = unique(yearcount);
% end;

hotyears = unique(yearcount);
k = 10; % what happend in the last k yers with heatwaves ?
saxqup = []; saxqlow = [];
for i = (length(hotyears) - k) : length(hotyears)
    yearn = hotyears(i);
    [aux1, aux2] = saxq4alphabet(yearn,option);
    saxqup = [saxqup aux1];
    saxqlow = [saxqlow aux2];
end;    
    
    
    

% plot(y)
% xlabel('Time');
% ylabel('Candidates to heatwaves');

% for i = 1 : 52
%     plot( y(i:i+10) ); hold on;
% end;
% hold off;

% nhw = size(HW.year1,2); % number of candidates to heat wave per year
% HW.year1{1}(:,1)   % from 1 to previously computed size: nhw % 1st column is time 2nd is tempt

