clear all
close all

%Load one weather file. In future... search in the HDD 'novos' folder
namedir1 = 'D:\novos\';        %    'H:\Data\novos\';       % Folder for picking up input data
namedir2 = 'D:\Output_ASAX\';  %    'H:\Data\Output_ASAX\'; % Folder for saving output CSV tables
myfiles = dir(strcat(namedir1,'*.csv'));

for i = 1:size(myfiles,1)
    fileAddress = strcat(namedir1,myfiles(i).name); %strcat('H:\Data\',myfiles(2).name); % my 2nd in H is AR000087828.csv
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    %fileAddress = 'C:\Users\AidaAraujo\Documents\sax_scripts\AR000087828.csv' ;
    
    dataYear = csvread(fileAddress,1,1);
    year = unique(dataYear(:,4)); % How many years do we have?
    nyears = length(year);
    
    features = []; % Table with the summary of the weather station
    
    for h = 1 : nyears
        ano = year(h);
        
        Tmax = dataYear(dataYear(:,4)== ano,8);
        Tmin = dataYear(dataYear(:,4)== ano,9);
        
        sdTmax = std(Tmax);
        if sdTmax < 2
            alphabet = 4;
        elseif sdTmax < 3
            alphabet = 5;
        else
            alphabet = 6;
        end
        
        minpeakdist = 1; % Tuning to avoid detecting too long candidates
        minpeakh = 2;  % Tuning to avoid detecting too long candidates
        
        [sax_max, locs] = adaptiveSAX1(Tmax, alphabet, minpeakdist, minpeakh);
        sax_min = adaptiveSAX1min(Tmin, alphabet, locs);
        
        %%%%%%%%% Detecting when sax_max and sax_min are top
        hot_x = warmer_ASAX(sax_max, sax_min, locs, alphabet);
        
        if isempty(hot_x)
            no_hot = sprintf('In %s the year %d does not have heat waves. \n', fileName, ano);
            disp(no_hot)
            % features = [features; ano, 0, 0, 0, 0, 0]; Add or not these years?
        else
            features = [features; features_ASAX(hot_x, ano, Tmax, Tmin)];
        end
    end
    
    s1 = strcat(namedir2, fileName, '_', 'output');
    s2 = '.csv';
    s  = strcat(s1,s2);
    csvwrite(s,features);
end