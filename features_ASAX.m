function features = features_ASAX(hot_x, ano, Tmax, Tmin)
% This function extracts features from the selected heat waves.

n_hw = size(hot_x,1);

start_hw = []; end_hw = []; y1 = []; y2 = [];
maxi = []; maximin = []; avgi = []; avgimin = [];
duration = [];

for i = 1:n_hw
   % y1 = [];
    x_hw = hot_x(i,1) : hot_x(i,2);
    start_hw = [start_hw; hot_x(i,1)]; 
    end_hw = [end_hw; hot_x(i,2)];
    y1 = [ y1; Tmax(x_hw) ];
    y2 = [ y2; Tmin(x_hw)];
    maxi = [maxi ; max(y1)];
    avgi = [avgi; mean(y1)];
    duration = [duration; length(x_hw)];
%     if duration < 3
%         p = [ p; 0 0 0];
%     else
%         p = [ p; polyfit(x_hw',Tmax(x_hw),2)];
%     end
end

ano = repmat(ano,n_hw,1);

features = [ano, duration, start_hw, end_hw, maxi, avgi, maximin, avgimin];