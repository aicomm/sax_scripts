clear all
close all

namedir1 = 'D:\Output_ASAX\'; % 'H:\Data\Output_ASAX\';
aa = read_mixed_csv2('H:\selectedStationsFile.csv',',',1);
aa(:,3) = strrep(aa(:,3),'"','');

bb = [];
for i = 2:size(aa,1)
    bb = [bb ; strcat(aa(i,3),'_output')];
end
        
myfiles = dir(strcat(namedir1,'*.csv'));

summary_70 = [];
for i = 2:size(myfiles,1)
    fileAddress = strcat(namedir1,myfiles(i).name); %strcat('H:\Data\',myfiles(2).name); % my 2nd in H is AR000087828.csv
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    if ~isempty(find(strncmp(bb, fileName, 12)))
        index = find(strncmp(bb, fileName, 12)); % compare the first 12 characteres (and find the match)
        latitude = str2num(cell2mat(aa(index,1)));
        longitude = str2num(cell2mat(aa(index,2)));
        try 
           data_summary = csvread(fileAddress,0,0);
           year70 = data_summary(data_summary(:,1) > 1969 & data_summary(:,1) < 1980,:); % selecting decade
           howmany = size(year70,1)/10;
           median_duration = median(year70(:,2));
           avg_max = mean(year70(:,5));
           avg_min = mean(year70(:,6));
           aux = [latitude, longitude, howmany, median_duration, avg_max, avg_min];
           target = strtok(fileName,'_');
           summary_a = [target, num2cell(aux)];
           summary_70 = [summary_70; summary_a]; % or using horzcat(A1,...,AN) ??
        catch err
        end
    else
        no_hot = sprintf('The weather station %s does not have records of heat wave. \n', fileName);
        disp(no_hot)
    end
end

% 4 tables summarising: 70's, 80's, 90's and 00's

T70 = cell2table(summary_70);
writetable(T70,'H:\Data\Output_ASAX\seventies.csv', 'WriteRowNames', false, 'WriteVariableNames', false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

summary_80 = [];
for i = 2:size(myfiles,1)
    fileAddress = strcat(namedir1,myfiles(i).name); %strcat('H:\Data\',myfiles(2).name); % my 2nd in H is AR000087828.csv
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    if ~isempty(find(strncmp(bb, fileName, 12)))
        index = find(strncmp(bb, fileName, 12)); % compare the first 12 characteres (and find the match)
        latitude = str2num(cell2mat(aa(index,1)));
        longitude = str2num(cell2mat(aa(index,2)));
        try 
           data_summary = csvread(fileAddress,0,0);
           year80 = data_summary(data_summary(:,1) > 1979 & data_summary(:,1) < 1990,:); % selecting decade
           howmany = size(year80,1)/10;
           median_duration = median(year80(:,2));
           avg_max = mean(year80(:,5));
           avg_min = mean(year80(:,6));
           aux = [latitude, longitude, howmany, median_duration, avg_max, avg_min];
           target = strtok(fileName,'_');
           summary_a = [target, num2cell(aux)];
           summary_80 = [summary_80; summary_a]; % or using horzcat(A1,...,AN) ??
        catch err
        end
    else
        no_hot = sprintf('The weather station %s does not have records of heat wave. \n', fileName);
        disp(no_hot)
    end
end

T80 = cell2table(summary_80);
writetable(T80,'H:\Data\Output_ASAX\eighties.csv', 'WriteRowNames', false, 'WriteVariableNames', false);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

summary_90 = [];
for i = 2:size(myfiles,1)
    fileAddress = strcat(namedir1,myfiles(i).name); %strcat('H:\Data\',myfiles(2).name); % my 2nd in H is AR000087828.csv
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    if ~isempty(find(strncmp(bb, fileName, 12)))
        index = find(strncmp(bb, fileName, 12)); % compare the first 12 characteres (and find the match)
        latitude = str2num(cell2mat(aa(index,1)));
        longitude = str2num(cell2mat(aa(index,2)));
        try 
           data_summary = csvread(fileAddress,0,0);
           year90 = data_summary(data_summary(:,1) > 1989 & data_summary(:,1) < 2000,:); % selecting decade
           howmany = size(year90,1)/10;
           median_duration = median(year90(:,2));
           avg_max = mean(year90(:,5));
           avg_min = mean(year90(:,6));
           aux = [latitude, longitude, howmany, median_duration, avg_max, avg_min];
           target = strtok(fileName,'_');
           summary_a = [target, num2cell(aux)];
           summary_90 = [summary_90; summary_a]; % or using horzcat(A1,...,AN) ??
        catch err
        end
    else
        no_hot = sprintf('The weather station %s does not have records of heat wave. \n', fileName);
        disp(no_hot)
    end
end

T90 = cell2table(summary_90);
writetable(T90,'H:\Data\Output_ASAX\nineties.csv', 'WriteRowNames', false, 'WriteVariableNames', false);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

summary_00 = [];
for i = 2:size(myfiles,1)
    fileAddress = strcat(namedir1,myfiles(i).name); %strcat('H:\Data\',myfiles(2).name); % my 2nd in H is AR000087828.csv
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    if ~isempty(find(strncmp(bb, fileName, 12)))
        index = find(strncmp(bb, fileName, 12)); % compare the first 12 characteres (and find the match)
        latitude = str2num(cell2mat(aa(index,1)));
        longitude = str2num(cell2mat(aa(index,2)));
        try 
           data_summary = csvread(fileAddress,0,0);
           year00 = data_summary(data_summary(:,1) > 1999 & data_summary(:,1) < 2010,:); % selecting decade
           howmany = size(year00,1)/10;
           median_duration = median(year00(:,2));
           avg_max = mean(year00(:,5));
           avg_min = mean(year00(:,6));
           aux = [latitude, longitude, howmany, median_duration, avg_max, avg_min];
           target = strtok(fileName,'_');
           summary_a = [target, num2cell(aux)];
           summary_00 = [summary_00; summary_a]; % or using horzcat(A1,...,AN) ??
        catch err
        end
    else
        no_hot = sprintf('The weather station %s does not have records of heat wave. \n', fileName);
        disp(no_hot)
    end
end

T00 = cell2table(summary_00);
writetable(T00,'H:\Data\Output_ASAX\millenium.csv', 'WriteRowNames', false, 'WriteVariableNames', false);