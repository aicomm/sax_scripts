%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a list with station name, lat and long to plout in arcgis or
% google earth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;close all
%dirCGE          = 'C:\Users\AidaAraujo\Documents\gce_datatools_396b_public\gce_sample_data\ghcnd_all';
%dirSAX          = 'C:\Users\AidaAraujo\Documents\sax_data';
dirNovos         = 'C:\Users\AidaAraujo\Documents\novos';
C                       = readtable('tbCountries.csv','Delimiter',',','ReadVariableNames',true);
countryId               = C.id;
countryName             = C.name;
countryNameAux          = repmat(' ',1,222);
f             = dir(dirNovos);
qtdStations   = length(f)-2; % the first 2 names are . and ..
disp qtdStations
tbAllStations = table([0.0; 0.0], [0.0;0.0],['AE000041196';'AE000041196'], [0;0], [0;0], [0;0], [0;0], [0;0], [0;0] , [0;0], [countryNameAux;countryNameAux], ...
                'VariableNames',{'latitude' 'longitude' 'name' 'yy_ini' 'mm_ini' 'dd_ini' 'yy_fin' 'mm_fi' 'dd_fi' 'qtd_years' 'countryname'});
tbSelStations = table([0.0; 0.0], [0.0;0.0],['AE000041196';'AE000041196'], [0;0], [0;0], [0;0], [0;0], [0;0], [0;0] , [0;0], [countryNameAux;countryNameAux], ...
                'VariableNames',{'latitude' 'longitude' 'name' 'yy_ini' 'mm_ini' 'dd_ini' 'yy_fin' 'mm_fi' 'dd_fi' 'qtd_years' 'countryname'});
            
indxAll       = 1;
indxSel       = 1;
dtInitial     = '19850101';
dtFinal       = '20151231';
numDtInitial  = datenum(dtInitial, 'yyyymmdd');
numDtFinal    = datenum(dtFinal, 'yyyymmdd');

for indx = 3:qtdStations; 
    nameValidFile               = strcat(dirNovos, '\', f(indx).name);
    T                           = readtable(nameValidFile,'Delimiter',',','ReadVariableNames',true);
    cdStation                   = T{1,1}; 
    idCountry                   = cdStation{1}(1:2);
    linCountry                  = strmatch(idCountry, countryId, 'exact'); 
    nmCountry                   = countryName(linCountry);
    tbAllStations(indxAll, 3)   = T{1,1};
    tbAllStations(indxAll, 1:2) = T(1,2:3);
    tbAllStations(indxAll, 4:6) = T(1,5:7); %Initial date
    tbAllStations(indxAll, 7:9) = T(end,5:7); %final date
    tbAllStations(indxAll, 10)  = array2table(table2array(T(end,5)) - table2array(T(1,5)) + 1); %final date
    countryNameAux              = repmat(' ',1,222);
    countryNameAux(1:length(nmCountry{1,1})) = nmCountry{1,1};
    tbAllStations(indxAll, 11)  = {countryNameAux(1:end)};
    stationIniDate              = table2array(T(1,4));
    stationFinDate              = table2array(T(end,4));
    if stationIniDate <= numDtInitial && stationFinDate >= numDtFinal
        tbSelStations(indxSel,:) = tbAllStations(indxAll,:);
        indxSel                  = indxSel +1;
    end;
    indxAll                     = indxAll+1;
end;
allStationsFile = strcat(dirNovos, '\allStationsFile.csv');
writetable(tbAllStations,allStationsFile, 'FileType','text','Delimiter', ',','QuoteStrings',true );
selStationsFile = strcat(dirNovos, '\selectedStationsFile.csv');
writetable(tbSelStations,selStationsFile, 'FileType','text','Delimiter', ',','QuoteStrings',true );
