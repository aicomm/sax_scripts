function sax_word = adaptiveSAX1min(tmin, alphabet, locs)
% timeseries is time sereries to be analysed by adaptive SAX
% alphabet is the alphabet SAX this is 4 or 5 by the moment
% minpeakdist and minpeakh are for calling peakseek (5 and 8 by the moment)
% tuning???
% call with aaword = adaptiveSAX1(Tmax, Tmin, 5, 5, 8);
% Adaptive SAX for Tmin conditioned to Tmax partition

% Tdata_max = tmax;
Tdata_min = tmin;

% locs contain the cut points to the adaptive PAA - these are related to
% the valleys (inverted series' peaks) we found in the time series
% invertedTdata = max(Tdata_max) - Tdata_max;                    %Aida
% locs = peakseek(invertedTdata, minpeakdist, minpeakh);         %Aida

data_len = size(Tdata_min,1);
% The next is used insted of nseg on classical SAX
% fac = factor(data_len);
% nseg          = 167; (?)  % the choice is just 2 and 167 in the case of
% AR00087828
%locs = [1; locs'; data_len];                                    %Aida
nseg = size(locs,1);
alphabet_size  = alphabet;  % alphabet size for SAX
% Note that alphabet size = 4 is fine when we focus only on summers. This
% has not enough sensitivity in the case of working with the whole year as
% winters would be 'a' and summers 'd' but does not distinguish any special
% event.

data = Tdata_min;
data = (data - mean(data))./std(data);

aux1 = [];
for i = 1:(nseg-1)
   aux1 = [aux1 ; mean(data(locs(i):locs(i+1)))];
end

% repeat mean value as many times as segment's length, that's all
aux2 = (repmat(aux1(1),1,(locs(2)-locs(1)+1)))';
for i = 2:(nseg-1)
   aux2 = [aux2 ; (repmat(aux1(i),1,(locs(i+1)-locs(i))))'];
end

% Adaptive PAA
aPAA = aux2;

% get the breakpoints
switch alphabet_size
    case 2, cutlines  = [0];
    case 3, cutlines  = [-0.43 0.43];
    case 4, cutlines  = [-0.67 0 0.67];
    case 5, cutlines  = [-0.84 -0.25 0.25 0.84];
    case 6, cutlines  = [-0.97 -0.43 0 0.43 0.97];
    case 7, cutlines  = [-1.07 -0.57 -0.18 0.18 0.57 1.07];
    case 8, cutlines  = [-1.15 -0.67 -0.32 0 0.32 0.67 1.15];
    case 9, cutlines  = [-1.22 -0.76 -0.43 -0.14 0.14 0.43 0.76 1.22];
    case 10, cutlines = [-1.28 -0.84 -0.52 -0.25 0. 0.25 0.52 0.84 1.28];
    otherwise, disp('WARNING:: Alphabet size too big');
end;

aux3 = [];
for i = 1:(nseg-1)
    aux3 = [aux3 ; aPAA(locs(i+1))];
end

lencut = size(cutlines,2);
aux5 = cell(1,nseg-1);
if lencut == 3   
    refa = aux3 <= cutlines(1);
    refb = find(aux3 > cutlines(1) & aux3 <=  cutlines(2));
    refc = find(aux3 >  cutlines(2) & aux3 <= cutlines(3));
    refd = find(aux3 > cutlines(3));
     
    aux5(refa) = {'a'};
    aux5(refb) = {'b'};
    aux5(refc) = {'c'};
    aux5(refd) = {'d'};
elseif lencut == 4
    refa = find(aux3 <= cutlines(1));
    refb = find(aux3 > cutlines(1) & aux3 <=  cutlines(2));
    refc = find(aux3 >  cutlines(2) & aux3 <= cutlines(3));
    refd = find(aux3 >  cutlines(3) & aux3 <= cutlines(4));
    refe = find(aux3 > cutlines(4));
    
    aux5(refa) = {'a'};
    aux5(refb) = {'b'};
    aux5(refc) = {'c'};
    aux5(refd) = {'d'};
    aux5(refe) = {'e'};  
elseif lencut == 5
    refa = find(aux3 <= cutlines(1));
    refb = find(aux3 > cutlines(1) & aux3 <=  cutlines(2));
    refc = find(aux3 >  cutlines(2) & aux3 <= cutlines(3));
    refd = find(aux3 >  cutlines(3) & aux3 <= cutlines(4));
    refe = find(aux3 >  cutlines(4) & aux3 <= cutlines(5));
    reff = find(aux3 > cutlines(5));
    
    aux5(refa) = {'a'};
    aux5(refb) = {'b'};
    aux5(refc) = {'c'};
    aux5(refd) = {'d'};
    aux5(refe) = {'e'};
    aux5(reff) = {'f'};

else disp('By the moment this only works for alphabet_size = 4, 5, and 6');
end

sax_word = aux5;

%sax_plot(cutlines, sax_word, aPAA, locs, data_len, nseg)