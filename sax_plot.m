function sax_plot(cutlines, sax_word, aPAA, locs, data_len, nseg)
str = sax_word;
% draw the gray guide lines in the background
guidelines = repmat(cutlines', 1, data_len);    
plot(guidelines', 'color', [0.8 0.8 0.8]);
hold on    
    
%pause;
    
color = {'r', 'm', 'g', 'y', 'c'};
% symbols = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
    
% high-light the segments and assign them to symbols
for i = 1 : (nseg - 1)
        
    % get the x coordinates for the segments
    x_start = locs(i);
    x_end   = locs(i+1);
    x_mid   = x_start + (x_end - x_start) / 2;

    % color-code each segment
    colorIndex = rem(char(str(i)),length(color))+1;
        
    % draw the segments
    plot(x_start:x_end,aPAA(x_start:x_end), 'color', color{colorIndex}, 'linewidth', 3);

    % show symbols
    text(x_mid, aPAA(x_start), str(i), 'fontsize', 14);
end

sax_string = str;




