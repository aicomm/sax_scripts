% clear all
close all

%Load one weather file. Search in the HDD 'novos' folder
fileName = 'H:\Data\AR000087828.csv'; % ARxxx is an Argentinian station
dataYear = csvread(fileName',1,1);

year = unique(dataYear(:,4)); % Which years do we have?

% First approach for just one of the years

Tmax = dataYear(dataYear(:,4)== year(6),8);
Tmin = dataYear(dataYear(:,4)== year(6),9);

% locs contain the cut points to the adaptive PAA - these are related to
% the valleys (inverted series' peaks) we found in the time series
invertedTmax = max(Tmax) - Tmax;
minpeakdist = 5;
minpeakh = 10;
locs = peakseek(invertedTmax, minpeakdist, minpeakh);

% findpeaks(invertedTmax, 'MinPeakDistance',4); % findpeaks(invertedTmax,'MinPeakProminence', 8,'Annotate', ...
                   %       'extents','WidthReference','halfheight');

data_len = size(Tmax,1);
% The next is used insted of nseg on classical SAX
% fac = factor(data_len);
% nseg          = 167; (?)  % the choice is just 2 and 167 in the case of
% AR00087828
locs = [1; locs'; data_len];
nseg = size(locs,1);
alphabet_size  = 5;  % alphabet size for SAX
% Note that alphabet size = 4 is fine when we focus only on summers. This
% has not enough sensitivity in the case of working with the whole year as
% winters would be 'a' and summers 'd' but does not distinguish any special
% event.

data = Tmax;
data = (data - mean(data))./std(data);
plot(data);

pause;

aux1 = [];
for i = 1:(nseg-1)
   aux1 = [aux1 ; mean(data(locs(i):locs(i+1)))];
end

% repeat mean value as many times as segment's length, that's all
aux2 = (repmat(aux1(1),1,(locs(2)-locs(1)+1)))';
for i = 2:(nseg-1)
   aux2 = [aux2 ; (repmat(aux1(i),1,(locs(i+1)-locs(i))))'];
end

% Adaptive PAA
aPAA = aux2;

hold on;
plot(aPAA,'r');
    
pause;

% get the breakpoints
switch alphabet_size
    case 2, cutlines  = [0];
    case 3, cutlines  = [-0.43 0.43];
    case 4, cutlines  = [-0.67 0 0.67];
    case 5, cutlines  = [-0.84 -0.25 0.25 0.84];
    case 6, cutlines  = [-0.97 -0.43 0 0.43 0.97];
    case 7, cutlines  = [-1.07 -0.57 -0.18 0.18 0.57 1.07];
    case 8, cutlines  = [-1.15 -0.67 -0.32 0 0.32 0.67 1.15];
    case 9, cutlines  = [-1.22 -0.76 -0.43 -0.14 0.14 0.43 0.76 1.22];
    case 10, cutlines = [-1.28 -0.84 -0.52 -0.25 0. 0.25 0.52 0.84 1.28];
    otherwise, disp('WARNING:: Alphabet size too big');
end;

% map the segments to string : WE SHALL WORK ON THIS !!!!!!!!
% This function only put an inverse order on the aPAA values
% str = timeseries2symbol(data, data_len, nseg, alphabet_size);

aux3 = [];
for i = 1:(nseg-1)
    aux3 = [aux3 ; aPAA(locs(i+1))];
end

lencut = size(cutlines,2);
aux5 = cell(1,nseg-1);
if lencut == 3   
    refa = find(aux3 <= cutlines(1));
    refb = find(aux3 > cutlines(1) & aux3 <=  cutlines(2));
    refc = find(aux3 >  cutlines(2) & aux3 <= cutlines(3));
    refd = find(aux3 > cutlines(3));
     
    aux5(refa) = {'a'};
    aux5(refb) = {'b'};
    aux5(refc) = {'c'};
    aux5(refd) = {'d'};
elseif lencut == 4
    refa = find(aux3 <= cutlines(1));
    refb = find(aux3 > cutlines(1) & aux3 <=  cutlines(2));
    refc = find(aux3 >  cutlines(2) & aux3 <= cutlines(3));
    refd = find(aux3 >  cutlines(3) & aux3 <= cutlines(4));
    refe = find(aux3 > cutlines(4));
    
    aux5(refa) = {'a'};
    aux5(refb) = {'b'};
    aux5(refc) = {'c'};
    aux5(refd) = {'d'};
    aux5(refe) = {'e'};  
else disp('By the moment this only works for alphabet_size = 4 or 5');
end

str = aux5;

% draw the gray guide lines in the background
guidelines = repmat(cutlines', 1, data_len);    
plot(guidelines', 'color', [0.8 0.8 0.8]);
hold on    
    
pause;
    
color = {'r', 'm', 'g', 'y', 'c'};
% symbols = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
    
% high-light the segments and assign them to symbols
for i = 1 : (nseg - 1)
        
    % get the x coordinates for the segments
    x_start = locs(i);
    x_end   = locs(i+1);
    x_mid   = x_start + (x_end - x_start) / 2;

    % color-code each segment
    colorIndex = rem(char(str(i)),length(color))+1;
        
    % draw the segments
    plot(x_start:x_end,aPAA(x_start:x_end), 'color', color{colorIndex}, 'linewidth', 3);

    % show symbols
    text(x_mid, aPAA(x_start), str(i), 'fontsize', 14);
end

sax_string = str;




