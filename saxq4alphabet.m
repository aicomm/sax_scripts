function [alphabetU, alphabetL] = saxq4alphabet (yearn, option)

if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;
if option == 1
   airtn = dataYear(dataYear(:,1)==yearn & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
else
   airtn = dataYear((dataYear(:,1)== yearn & dataYear(:,2)>=10) | (dataYear(:,1)== yearn + 1 & dataYear(:,2)<=3),9); % summer temperature h_th year in Recife
end
 
data = airtn;
if length(data) >= 540
    data = data(1:540);
else data = data(1:504);
end;

data_len = length(data);
% data = data(1:data_len-1);
if option == 1
        nseg = 9;  % number of segments for PAA approach: 1 3 9 61 183 549  (61)
    else
        nseg = 18;  % number of segments for PAA approach in the case of Recife summer: 2 3 7 13 78 546 (78)
end;
  
alphabet_size = 4; % alphabet size for SAX

% nseg must be divisible by data length
if (mod(data_len, nseg))       
    disp('nseg must be divisible by the data length. Aborting ');
    return;          
end;

win_size = floor(data_len/nseg);
    
data = (data - mean(data))/std(data);

PAAql = quantile(reshape(data,win_size,nseg),0.05);
PAAqu = quantile(reshape(data,win_size,nseg),0.95);

PAAql_plot = repmat(PAAql', 1, win_size);
PAAql_plot = reshape(PAAql_plot', 1, data_len)';

PAAqu_plot = repmat(PAAqu', 1, win_size);
PAAqu_plot = reshape(PAAqu_plot', 1, data_len)';
    
dataql = (PAAql_plot - mean(PAAql_plot))/std(PAAql_plot);
dataqu = (PAAqu_plot - mean(PAAqu_plot))/std(PAAqu_plot);
strql = timeseries2symbol(dataql, data_len, nseg, alphabet_size);
strqu = timeseries2symbol(dataqu, data_len, nseg, alphabet_size);

symbols = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};

sU = symbols(strqu); alphabetU = sU(:);
sL = symbols(strql); alphabetL = sL(:);