clear all
close all

%Load one weather file. In future... search in the HDD 'novos' folder
namedir1 = 'C:\Users\Aida\Downloads\ASAX_0\Data\novos\Base_completa_com_Ref_100m_2014_Mensal\'; % 'C:\Data\'; % Folder for picking up input data
namedir2 = 'C:\Users\Aida\Downloads\ASAX_0\Data\Output_ASAX_min\';   % Folder for saving output CSV tables
myfiles = dir(strcat(namedir1,'*.mat'));

for i = 1:size(myfiles,1)
     
    %fileAddress = ('C:\Users\Aida\Downloads\ASAX_0\Data\novos\Base_completa_com_Ref_100m_2014_Mensal\Base_com_Ref_100m_2014_MO_01.mat');
    fileAddress = strcat(namedir1,myfiles(i).name); %Para pegar todos os arquivos do diretório 
    [pathstr, fileName, ext] = fileparts(fileAddress);
    
    load(  fileAddress); %Base de ventos
    dataYear = eval(fileName);
    year = unique(dataYear(:,3)); % How many years do we have?
    nyears = length(year);
    
    features = []; % Table with the summary of the weather station
    
    for h = 1 : nyears
        ano = year(h);
        
        Tspeed = dataYear(dataYear(:,3)== ano,14); %wind speed
        Tmonth = dataYear(dataYear(:,3)== ano,2); %month
        Tday = dataYear(dataYear(:,3)== ano,1); %day
        Thour = dataYear(dataYear(:,3)== ano,4); %hour
        
        sdTmax = std(Tspeed);
        if sdTmax < 3
            alphabet = 4;
        elseif sdTmax < 6
            alphabet = 5;
        else
            alphabet = 6;
        end
        
        minpeakdist = 3; % Tuning to avoid detecting too long candidates
        minpeakh = 6;  % Tuning to avoid detecting too long candidates
        
        %[sax_max, locs] = adaptiveSAX1(Tmax, alphabet, minpeakdist, minpeakh);
        [sax_aux, locs] = adaptiveSAX1wind(Tspeed, alphabet, minpeakdist, minpeakh);
        sax_min = adaptiveSAX1min(Tspeed, alphabet, locs);
        
        %%%%%%%%% Detecting when sax_max and sax_min are top
        %hot_x = warmer_ASAX(sax_max, sax_min, locs, alphabet);
        hot_x = colder_ASAX(sax_min, locs, alphabet);
        
        if isempty(hot_x)
            no_hot = sprintf('In %s the year %d does not have heat waves. \n', fileName, ano);
            disp(no_hot)
            % features = [features; ano, 0, 0, 0, 0, 0]; Add or not these years?
        else
            features = [features; features_ASAX1(hot_x, ano, Tspeed, Tmonth, Tday, Thour )];
        end
    end
    
    s1 = strcat(namedir2, fileName, '_', 'output');
    s2 = '.csv';
    s  = strcat(s1,s2);
    csvwrite(s,features);
end