%Read header
fid = fopen('recife3linesv1.csv');
a = textscan(fid,'%s','Delimiter','\n');
b = a{1}(1);
fclose(fid);

%Filer file
max_temp  = 45;
min_temp  = 14;
mean_temp = [26.5  26.5  26.4  25.9  25.2  24.5  23.9  23.9  24.6  25.5  26.1  26.4];
dataYear  = csvread('recife3linesv1.csv',1,0);  
data_len  = length(dataYear);
cont      = 0
for ind = 1:data_len
    if dataYear(ind,9) < min_temp || dataYear(ind,9) > max_temp
        dataYear(ind, 9) = mean_temp(dataYear(ind, 2));
        cont = cont +1;
    end
end
disp(cont)
%csvwrite('recife3linesv2.csv',dataYear);  
%Write with header
csvwrite_with_headers('recife3linesv2.csv',dataYear,b)