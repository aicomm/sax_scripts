%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Importing data from weather stations. 1-London dataset and 2-Recife
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all; clear all; clc
option = 2; %1-London datset and 2-Recife
% Starting to work for AIComm idea on multiresolution time series using SAX
% Let's see ...
if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;
if option == 1
   airt89 = dataYear(dataYear(:,1)==1989 & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
else
   airt89 = dataYear((dataYear(:,1)== 1977 & dataYear(:,2)>=10) | (dataYear(:,1)== 1978 & dataYear(:,2)<=3),9); % summer temperature h_th year in Recife
 end
data = airt89;
data = data(1:540);

data_len = length(data);
% data = data(1:data_len-1);
nseg          = 9; % number of segments for PAA approach: 1 3 9 61 183 549
alphabet_size = 4; % alphabet size for SAX

% nseg must be divisible by data length
if (mod(data_len, nseg))
        
    disp('nseg must be divisible by the data length. Aborting ');
    return;  
        
end;

win_size = floor(data_len/nseg);
    
data = (data - mean(data))/std(data);
plot(data);
axis([0,550,-3,3])
xlabel('Time'); %: from early June to early September');
ylabel('Temperature excursion - Celsius');
pause;
    
% special case: no dimensionality reduction
if data_len == nseg
    PAA = data;
        
% Convert to PAA.  Note that this line is also in timeseries2symbol, which will be
% called later.  So it's redundant here and is for the purpose of plotting only.
else
    PAA = [mean(reshape(data,win_size,nseg))];                     
end
    
% plot the PAA segments
PAA_plot = repmat(PAA', 1, win_size);
PAA_plot = reshape(PAA_plot', 1, data_len)';
    
hold on;
plot(PAA_plot,'r');

pause;

str = timeseries2symbol(data, data_len, nseg, alphabet_size);

% get the breakpoints
switch alphabet_size
    case 2, cutlines  = [0];
    case 3, cutlines  = [-0.43 0.43];
    case 4, cutlines  = [-0.67 0 0.67];
    case 5, cutlines  = [-0.84 -0.25 0.25 0.84];
    case 6, cutlines  = [-0.97 -0.43 0 0.43 0.97];
    case 7, cutlines  = [-1.07 -0.57 -0.18 0.18 0.57 1.07];
    case 8, cutlines  = [-1.15 -0.67 -0.32 0 0.32 0.67 1.15];
    case 9, cutlines  = [-1.22 -0.76 -0.43 -0.14 0.14 0.43 0.76 1.22];
    case 10, cutlines = [-1.28 -0.84 -0.52 -0.25 0. 0.25 0.52 0.84 1.28];
    otherwise, disp('WARNING:: Alphabet size too big');
end;

% draw the gray guide lines in the background
guidelines = repmat(cutlines', 1, data_len);    
plot(guidelines', 'color', [0.8 0.8 0.8]);
hold on    
    
pause;

color = {'g', 'y', 'm', 'c'};
symbols = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
    
% high-light the segments and assign them to symbols
for i = 1 : nseg
        
   % get the x coordinates for the segments
   x_start = (i-1) * win_size + 1;
   x_end   = x_start + win_size - 1;
   x_mid   = x_start + (x_end - x_start) / 2;

   % color-code each segment
   colorIndex = rem(str(i),length(color))+1;
        
   % draw the segments
   plot([x_start:x_end],PAA_plot([x_start:x_end]), 'color', color{colorIndex}, 'linewidth', 3);

   % show symbols
   text(x_mid, PAA_plot(x_start), symbols{str(i)}, 'fontsize', 14);
end

sax_string = symbols(str);
           
mdl = fitlm(1:length(airt89),airt89,'linear','RobustOpts','on');