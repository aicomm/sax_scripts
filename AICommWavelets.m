option = 2; %1-London datset and 2-Recife
% Starting to work for AIComm idea on multiresolution time series using SAX
% Let's see ...
if ispc
  machinename = getenv('COMPUTERNAME');
else
  machinename = getenv('HOSTNAME');
end
if strcmp(machinename,'UFPECHESF') == 1
    if option == 1
       dataYear = csvread('london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('recife3linesv2.csv',1,0); % it doesn't include 3(4) variables 
    end;
else
    if option == 1
       dataYear = csvread('H:\Data\london3linesv1.csv',1,0); % it doesn't include 3(4) variables
    else
       dataYear = csvread('H:\Data\recife3linesv2.csv',1,0); % it doesn't include 3(4) variables
    end; 
end;
if option == 1
   airt89 = dataYear(dataYear(:,1)==1989 & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
else
   airt89 = dataYear(dataYear(:,1)==1989 & dataYear(:,2)>=4 & dataYear(:,2)<=9,9); % air temperature for 1989
   %airt89 = dataYear(dataYear(:,1)==1989 & (dataYear(:,2)<=3 | dataYear(:,2)>=10),9); % summer temperature 1989 in Recife
end
data = airt89;

data_len = length(data);

resolwt = [8 4 2];
s = data;

aux = [];
for i = 1:length(resolwt)
    approx = resolwt(i);
    [C,L] = wavedec(s,approx,'db1');
    Awt = wrcoef('a',C,L,'db1',approx);
    aux = [aux Awt];
end

figure
subplot(2,2,1)       % add first plot in 2 x 2 grid
plot(data)           % line plot
if option == 1
   axis([0,550,0,35])
else
   axis([0,550,20,35])
end; 
title('Summer temperature data')

subplot(2,2,2)       % add second plot in 2 x 2 grid
plot(aux(:,1))           % scatter(x,y2) % scatter plot
if option == 1
   axis([0,550,0,35])
else
   axis([0,550,20,35])
end; 
title('Wavelet at resolution R1')

subplot(2,2,3)       % add third plot in 2 x 2 grid
plot(aux(:,2))       % stem(x,y1)  % stem plot
if option == 1
   axis([0,550,0,35])
else
   axis([0,550,20,35])
end; 
title('Wavelet at resolution R2')

subplot(2,2,4)       % add third plot in 2 x 2 grid
plot(aux(:,3))       % stem(x,y1)  % stem plot
if option == 1
   axis([0,550,0,35])
else
   axis([0,550,20,35])
end; 
title('Wavelet at resolution R3')

% subplot(2,2,4)       % add fourth plot in 2 x 2 grid
% yyaxis left          % plot against left y-axis
% plot(x,y1)
% yyaxis right         % plot against right y-axis
% plot(x,y2)
% title('Wavelet resolution R3')