%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a file with station name, lat and long and temperature 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 clear all;close all
%dirCGE          = 'C:\Users\AidaAraujo\Documents\gce_datatools_396b_public\gce_sample_data\ghcnd_all';
  dirSAX           = 'H:\gce_datatools_396b_public';
  dirNovos         = 'D:\ghcnd_main_data\novos';
  f                = dir(dirNovos);
  qtdStations      = length(f)-2; % the first 2 names are . and ..
  disp qtdStations
  tbtempStations   = table(['AE000041196' ;'AE000041196'], [0.0; 0.0], [0.0;0.0], [0;0], [0;0], [0;0], [0;0], [0.0;0.0], [0.0;0.0], [0.0;0.0], ...
                   'VariableNames',{'tstation' 'latitude' 'longitude' 'tdate' 'tyear' 'tmonth' 'tday' 'Tavg' 'Tmax' 'Tmin'});
  nameSelectFile   = strcat(dirSAX, '\', 'selectedStationsFile.csv');
  TBSelSta         = readtable(nameSelectFile,'Delimiter',',','ReadVariableNames',true);
              
  [qtdStations, qtdCol] = size(TBSelSta);
  indxSel          = 1;
  dtInitial        = '19850101';
  dtFinal          = '20151231';
  numDtInitial     = datenum(dtInitial, 'yyyymmdd')-1;
  numDtFinal       = datenum(dtFinal, 'yyyymmdd')+1;
  ini              = 0;
  final            = 0;
for ini = 1:qtdStations; 
    nameValidFile               = strcat(dirNovos, '\', TBSelSta.name{ini}, '.csv'); % indx
    T                           = readtable(nameValidFile,'Delimiter',',','ReadVariableNames',true);
    clear tbAux;
    
    tbAux                          = T(T{:,4} > numDtInitial & T{:,4} < numDtFinal,:);
    [lin, col]                     = size (tbAux);
    ini                            = final + 1;
    final                          = ini + lin - 1;
    tbtempStations(ini:final,2:10) = tbAux(:,2:10);
    b                              = cellstr(tbAux{:,1});
    tbtempStations(ini:final,1).tstation = char(b(1:end,1));
    fprintf(' ind:%i station: %s ', ini, char(b(1,1))  );  % indx

end;
tempStationsFile = strcat(dirSAX, '\tempStationsFile.csv');
writetable(tbtempStations,tempStationsFile, 'FileType','text','Delimiter', ',','QuoteStrings',true );
