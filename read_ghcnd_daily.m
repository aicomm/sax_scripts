% [s,msg] = imp_ascii('weather.txt','d:\data\met','Weather Data','weather_template');
% [s,msg] = clearflags(s,'I');
% msg = exp_ascii(s,'tab','weather_qc.txt','d:\data\met','Weather Data','ST','M','FLED');

%
%input:
%  fn = name of the data file to import
%  pn = path of the data file to import (default = pwd)
%  template = metadata template to use (default = 'NCDC_GHCND')
%  datestart = beginning year and month to return (YYYYMM, default = [] for earliest available)
%  dateend = ending year and month to return (YYYYMM, default = [] for latest available)
%  silent = option to suppress progress bar status updates if GUI mode (0 = no/default, 1 = yes)
%  deleteopt = option to delete the temporary tabular file (0 = no, 1 = yes/default)
%
%output:
%  s = GCE data structure
%  msg = text of any error messages
%  s_raw = initial GCE data structure prior to denormalization and application of attribute metadata
%
%notes:
%  1) Parameter definitions in 'ncdc_ghcnd_parameters.mat' are used to translate integers
%     into standard numeric fields and to apply parameter-specific metadata to attributes
%  2) A monotonic time series data set will be generated for the full period of record
%  3) Only quality control information in the QFlags columns will be preserved as QA/QC flags
%  4) The normalized raw table (s_raw) only contains records for non-null (non-NaN) observations
%  5) A TMEAN column (mean daily temperature) will be calculated automatically from TMIN and TMAX
%     if TAVG (measured average temperature) is not present
%
clear all;close all
qtdMinYear = 29;
indFileOk   = 1;
dailyStationFile='DailyStations.xlsx';
%v = load('ncdc_ghcnd_stations.mat','-mat'); %settings -> Para carregar o nome dos pa�ses
%stationidV = cellstr(v.data.values{1,1});
%countryV   = cellstr(v.data.values{1,3});
%flagmeta = lookupmeta(parmlist,'Data','Codes');
%stationid  = cellstr(parmlist.values{1,1});
C = readtable('tbCountries.csv','Delimiter',',','ReadVariableNames',true);
countryId   = C.id;
countryName = C.name;
T = readtable('tbStations.csv','Delimiter',',','ReadVariableNames',true);
parmlist    = table2struct(T,'ToScalar',true);
stationidT  = parmlist.stationid;
%lat        = parmlist.values{1,7};
latT        = parmlist.lat;
%long       = parmlist.values{1,8};
longT        = parmlist.long;

pn          = 'C:\Users\AidaAraujo\Documents\gce_datatools_396b_public\gce_sample_data\ghcnd_all';
template    = 'NCDC_GHCND';
datestart   = [];
dateend     = [];
silent      = 0; %suppress progress bar status updates if GUI mode (0 = no/default, 1 = yes)
deleteopt   = 1; %delete the temporary tabular file (0 = no, 1 = yes/default)

f = dir (pn);
for x = 1: 20;%length(f); 
    tipo = findstr(f(x).name, 'dly');
    tipoTable = findstr(f(x).name, 'table');
    if ~isempty(tipo) && isempty(tipoTable)
       fn1           = f(x).name;
       [s,msg,s_raw] = imp_ncdc_ghcnd(fn1,pn,template,datestart,dateend,silent,deleteopt);
       %cdStation = unique(extract(s_raw,'Station'));
       cdStation     = unique(extract(s,'Station')); 
       idCountry     = cdStation{1}(1:2);
       linCountry    = strmatch(idCountry, countryId, 'exact'); 
       nmCountry     = countryName(linCountry);
       cdLin         = strmatch(cdStation, stationidT, 'exact'); 
       cdLat         = latT(cdLin,1); 
       cdLong        = longT(cdLin,1); 
              
       dt_str = extract(s_raw,'Date');
       %get numeric serial dates
       dt_str = extract(s_raw,'Date');
       if mlversion >= 8
          %use format-specific datenum
          dt = datenum(dt_str,'mm/dd/yyyy');
       else
          %use generic datenum
          dt = datenum(dt_str);
       end
              
       %get date range
       dt_min  = min(no_nan(dt));
       iniYear = str2num(datestr(dt_min,'yyyy')); 
       dt_max = max(no_nan(dt));
       finalYear = str2num(datestr(dt_max,'yyyy')); 
       qtdYear = finalYear - iniYear;
       if  qtdYear > qtdMinYear
           qtdYearOk = 1;
       else
           qtdYearOk = 0;
       end;
       %generate complete date range and formatted string version
       dt_full = (dt_min:dt_max)';
               
       %get list of all unique parameter codes
       parms_all = extract(s_raw,'Parameter');
       parms = unique(parms_all);
       existTMAX = strmatch('TMAX', parms, 'exact')>0; 
       existTMIN = strmatch('TMIN', parms, 'exact')>0; 
       if qtdYearOk == 1 && ~isempty(existTMAX) && ~isempty(existTMIN) && ~isempty(cdLat) && ~isempty(cdLong)
          fileOK = 1;
          statioIdOK{indFileOk,1} = cdStation;
          %Collect the valid stations
          latitude(indFileOk,1)     = cdLat;
          longitude(indFileOk,1)    = cdLong;
          name(indFileOk,1) = cdStation(1,1);
          ctavg=strmatch('TAVG', s.name, 'exact');
          ctmax=strmatch('TMAX', s.name, 'exact');
          ctmin=strmatch('TMIN', s.name, 'exact');
          tableStationTemperature = table (name, latitude, longitude,
          indFileOk = indFileOk +1;
          
       else
          fileOK = 0;
       end;
       str = sub_format_coords(cdLong,cdLat);
       fprintf('ok: %i ind:%i station: %s Country:%s coordinate:%s  ', fileOK, x, cdStation{1,1},  nmCountry{1});
       fprintf(' lat:%f   long:%f  qtdYear:%i   exist TMAX:%i  exist TMIN:%i  qtd Ok:%i \n',   cdLat, cdLong, qtdYear, existTMAX, existTMIN, (indFileOk-1));
       %fprintf('station:%s %s \n', s.metadata{18,3}, s.metadata{19,3});
    end;
end;

tableStation = table( name, latitude, longitude);
writetable(tableStation,dailyStationFile, 'FileType', 'spreadsheet');
